/********************************************************************************
** Form generated from reading UI file 'window.ui'
**
** Created by: Qt User Interface Compiler version 5.12.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WINDOW_H
#define UI_WINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralwidget;
    QGridLayout *gridLayout;
    QLabel *exp_simb;
    QPushButton *quit;
    QPushButton *calcButton;
    QLabel *post_simb;
    QLabel *logo;
    QLineEdit *iExpr;
    QLabel *nombre;
    QLabel *oExpr;
    QLabel *result;
    QLabel *igual;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(616, 435);
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        gridLayout = new QGridLayout(centralwidget);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        exp_simb = new QLabel(centralwidget);
        exp_simb->setObjectName(QString::fromUtf8("exp_simb"));
        QFont font;
        font.setPointSize(12);
        font.setUnderline(true);
        exp_simb->setFont(font);

        gridLayout->addWidget(exp_simb, 0, 1, 1, 1);

        quit = new QPushButton(centralwidget);
        quit->setObjectName(QString::fromUtf8("quit"));
        QFont font1;
        font1.setPointSize(12);
        quit->setFont(font1);

        gridLayout->addWidget(quit, 8, 3, 1, 1);

        calcButton = new QPushButton(centralwidget);
        calcButton->setObjectName(QString::fromUtf8("calcButton"));
        calcButton->setFont(font1);
        calcButton->setFlat(false);

        gridLayout->addWidget(calcButton, 2, 3, 1, 1);

        post_simb = new QLabel(centralwidget);
        post_simb->setObjectName(QString::fromUtf8("post_simb"));
        post_simb->setFont(font);

        gridLayout->addWidget(post_simb, 4, 1, 1, 1);

        logo = new QLabel(centralwidget);
        logo->setObjectName(QString::fromUtf8("logo"));
        logo->setMaximumSize(QSize(100, 50));
        logo->setPixmap(QPixmap(QString::fromUtf8(":/img/Logo_UACJ.png")));
        logo->setScaledContents(true);

        gridLayout->addWidget(logo, 0, 3, 1, 1);

        iExpr = new QLineEdit(centralwidget);
        iExpr->setObjectName(QString::fromUtf8("iExpr"));
        iExpr->setFont(font1);
        iExpr->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(iExpr, 3, 1, 1, 1);

        nombre = new QLabel(centralwidget);
        nombre->setObjectName(QString::fromUtf8("nombre"));

        gridLayout->addWidget(nombre, 8, 1, 1, 1);

        oExpr = new QLabel(centralwidget);
        oExpr->setObjectName(QString::fromUtf8("oExpr"));
        QFont font2;
        font2.setPointSize(15);
        oExpr->setFont(font2);
        oExpr->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(oExpr, 5, 1, 1, 1);

        result = new QLabel(centralwidget);
        result->setObjectName(QString::fromUtf8("result"));
        QFont font3;
        font3.setPointSize(14);
        result->setFont(font3);
        result->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(result, 5, 3, 1, 1);

        igual = new QLabel(centralwidget);
        igual->setObjectName(QString::fromUtf8("igual"));
        QFont font4;
        font4.setPointSize(19);
        igual->setFont(font4);
        igual->setAlignment(Qt::AlignCenter);
        igual->setTextInteractionFlags(Qt::NoTextInteraction);

        gridLayout->addWidget(igual, 5, 2, 1, 1);

        MainWindow->setCentralWidget(centralwidget);

        retranslateUi(MainWindow);

        calcButton->setDefault(false);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", nullptr));
        exp_simb->setText(QApplication::translate("MainWindow", "Expresion", nullptr));
        quit->setText(QApplication::translate("MainWindow", "Salir", nullptr));
        calcButton->setText(QApplication::translate("MainWindow", "Calcular", nullptr));
        post_simb->setText(QApplication::translate("MainWindow", " Posfija", nullptr));
        logo->setText(QString());
        iExpr->setPlaceholderText(QApplication::translate("MainWindow", "Introduzca una operacion", nullptr));
        nombre->setText(QApplication::translate("MainWindow", "Pablo Barraza Cornejo 164006", nullptr));
        oExpr->setText(QApplication::translate("MainWindow", "Notacion Postfija Polaca", nullptr));
        result->setText(QApplication::translate("MainWindow", "Resultado", nullptr));
        igual->setText(QApplication::translate("MainWindow", "=", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WINDOW_H
