#include "exp_balanceada.hpp"
#include "pila_din.hpp"

bool is_operator(char iChar)
{
  if((iChar == '+') ||(iChar== '-') || (iChar== '/')|| (iChar == '*') || (iChar == '(') || iChar == ')')
    return true;
  return false;
}

bool is_operator_low(char iChar, char iChar_Pila)
{
  if(iChar_Pila=='(')
    return false;
  if(iChar < iChar_Pila)
    return false;
  return true;
}

int calculo(char iOptr, int a_num, int b_num)
{
  switch(iOptr)
    {
    case '+':
      a_num += b_num;
      break;
    case '*':
      a_num *= b_num;
      break;
    case'/':
      a_num = a_num/b_num;
      break;
    case'-':
      a_num-=b_num;
      break;
    }
  return a_num;
}


int process_postfix(std::string iExpression)
{
  PilaDin<int> Pila;
  PilaDin<int> *tope = NULL;
  unsigned int i;
  int temp;
  if(iExpression==" ")
      return 0;
  for(i = 0; i < iExpression.length(); i++){
    if(isdigit(iExpression[i])){
      Pila.push(&tope, (iExpression[i]%48)); //Mete solo los operandos al stack
    }else if (is_operator(iExpression[i])) { /*Si encuentra un operador,
					       hace la operacion con los ultimos 2 operandos
					       y pone el resultado en la pila */
      temp = calculo(iExpression[i], Pila.pop(&tope), Pila.pop(&tope));
      Pila.push(&tope, temp);
    }
  }
  return Pila.pop(&tope);
}

std::string infix_to_postfix(std::string iExpression)
{
  PilaDin<char> Pila;
  PilaDin<char> *tope = NULL;
  unsigned int i;
  std::string oExpression;
  if(!balance(iExpression))
    return " ";
  for( i = 0; i < iExpression.length(); i++){
    if(iExpression[i] ==' ') //Elimina los espacios de la expresion
	  continue;
    if(isdigit(iExpression[i])){
      oExpression += iExpression[i]; //Imprime a los operandos
    }
    else{
      if (iExpression[i] == ')') {
	while (Pila.mostrarTope(tope) != '(') 
	  oExpression += Pila.pop(&tope); /*Si encuentra un ')', 
					    saca todos los contenidos de
					    la pila y los imprime */
	Pila.pop(&tope); //Saca pero no imprime el '('
      }else{
	while(is_operator_low(iExpression[i], Pila.mostrarTope(tope)) && !Pila.checaXvacio(tope)){
	  oExpression += Pila.pop(&tope);
	}
	Pila.push(&tope, iExpression[i]);
      }
      }
  }
  while(!Pila.checaXvacio(tope))
    oExpression += Pila.pop(&tope);
  return oExpression;
}
