#include "window.h"
#include "ui_window.h"
#include "post_fix.hpp"
#include <QTextStream>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
    {
        ui->setupUi(this);
        connect(ui->quit, SIGNAL(clicked()), QApplication::instance(), SLOT (quit()));
        connect(ui->iExpr, SIGNAL(returnPressed()), this, SLOT(get_post_exp()));
        connect(ui->calcButton, SIGNAL(clicked()), this, SLOT(get_post_exp()));
    }

    MainWindow::~MainWindow()
    {
        delete ui;
    }

void MainWindow::set_exp(QString iExp)
{
    ui->iExpr->setText(iExp);
}
void MainWindow::get_post_exp()
{
    QString post_exp;
    QString res;
    ui->oExpr->setText(post_exp.fromStdString(infix_to_postfix(ui->iExpr->text().toStdString())));
    res.setNum(process_postfix(ui->oExpr->text().toStdString()));
    ui->result->setText(res);
}
