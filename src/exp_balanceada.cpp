#include "exp_balanceada.hpp"
#include "pila_din.hpp"

bool simb_abierto(char iSimbolo)
{
  if(iSimbolo == '{' || iSimbolo == '[' || iSimbolo == '(')
    return true;
  return false;
}
bool simb_cerrado(char iSimbolo)
{
  if(iSimbolo == ')' || iSimbolo == ']' || iSimbolo == '}')
    return true;
  return false;
}
bool concuerdan(char iSimboloAbierto, char iSimboloCerrado)
{
  return (((iSimboloAbierto == '(') && iSimboloCerrado == ')')
	  || ((iSimboloAbierto == '{') && iSimboloCerrado == '}')
	  || ((iSimboloAbierto == '[') && iSimboloCerrado == ']'));
}
bool balance(std::string input_String)
{
  PilaDin<char> MiPila;
  PilaDin<char> *topePila = NULL;
  bool balance = true;
  unsigned int i;
  char Simb_Abierto;
  for( i = 0; i < input_String.length(); i++){
    if(simb_abierto(input_String[i]))
      MiPila.push(&topePila, input_String[i]);
    else if(simb_cerrado(input_String[i])){
      if(MiPila.checaXvacio(topePila))
	balance = false;
      else{
	Simb_Abierto = MiPila.mostrarTope(topePila);
	MiPila.pop(&topePila);
	balance = concuerdan(Simb_Abierto,input_String[i]);
      }
    }
  }
  if(MiPila.checaXvacio(topePila))
    return balance;
  return false;
}
