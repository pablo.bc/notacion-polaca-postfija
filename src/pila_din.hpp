#ifndef C_PILADIN
#define C_PILADIN
#include<iostream>

template <class T>
class PilaDin {
private:
  T dato;
  PilaDin* enlace;
public:
  void push(PilaDin **, T);
  T pop(PilaDin **);
  bool checaXlleno(PilaDin *);
  bool checaXvacio(PilaDin *);
  void mostrarPila(PilaDin *);
  T mostrarTope(PilaDin *);
};

template <class T>
void PilaDin<T>::push(PilaDin **tope, T iDato)
{
  PilaDin *temp;
  temp = new PilaDin();
  temp->dato = iDato;
  if(checaXlleno(temp)){
    temp->enlace = *tope;
    *tope = temp;
  }
}
template <class T>
T PilaDin<T>::pop(PilaDin **tope)
{
  PilaDin *temp = *tope;
  if(checaXvacio(*tope))
    return ' ';
  dato = (*tope)->dato;
  *tope = (*tope)->enlace;
  delete temp;
  return dato;
}
template <class T>
bool PilaDin<T>::checaXlleno(PilaDin *tope)
{
  return tope != NULL;
}
template <class T>
bool PilaDin<T>::checaXvacio(PilaDin *tope)
{
  return tope == NULL;
}
template <class T>
void PilaDin<T>::mostrarPila(PilaDin *tope)
{
  PilaDin *temp = tope;
  while(temp->enlace != NULL){
    std::cout << temp->dato << std::endl;
    temp = temp->enlace;
  }
  std::cout<<temp->dato<<std::endl;
}
template <class T>
T PilaDin<T>::mostrarTope(PilaDin *tope)
{
  if(checaXvacio(tope))
    return ' ';
  return tope->dato;
}
#endif //C_PILADIN

