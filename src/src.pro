TEMPLATE = app
TARGET = NotacionPolaca

QT = core gui
DESTDIR = ../release
OBJECTS_DIR = ../tmp

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

HEADERS += pila_din.hpp window.h post_fix.hpp exp_balanceada.hpp

SOURCES += main.cpp \
window.cpp \
exp_balanceada.cpp

FORMS += \
window.ui

RESOURCES = main.qrc
